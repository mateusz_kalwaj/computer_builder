class ZestawABC996 extends Builder {

    public void buildMonitor() {
        zestawKomputerowy.setMonitor("LG");
    }

    public void buildProcesor() {
        zestawKomputerowy.setProcesor("Intel");
    }

    public void buildGrafika() {
        //brak grafiki

    }

    public void buildRam() {
        zestawKomputerowy.setRam("DDR");

    }

    public void buildHdd() {
        zestawKomputerowy.setHdd("Samsung");

    }
}
