class Director {

    //kierownik budowy!

    private Builder builder;

    public void setBuilder(Builder builder) {
        this.builder = builder;
    }

    public ZestawKomputerowy getZestaw() {
        return builder.getZestaw();
    }

    public void skladaj() {
        builder.nowyZestaw();
        builder.buildMonitor();
        builder.buildProcesor();
        builder.buildHdd();
        builder.buildRam();
        builder.buildGrafika();
    }
}
